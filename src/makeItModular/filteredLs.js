const fs = require('fs');

module.exports = function filteredLs(directoryName, fileExtension, callback) {
  const extensionName = `.${fileExtension}`;

  fs.readdir(directoryName, 'utf8', (err, files) => {
    if (err) {
      return callback(err);
    }

    const filteredFiles = files.filter(val => val.endsWith(extensionName));
    return callback(null, filteredFiles);
  });
};
