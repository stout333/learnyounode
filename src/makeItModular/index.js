import filteredLs from './filteredLs';

const directoryName = process.argv[2];
const extensionName = process.argv[3];

const lsCallBack = (error, data) => {
  if (error) {
    console.log(error);
  } else {
    data.forEach((element) => {
      console.log(element);
    });
  }
};

filteredLs(directoryName, extensionName, lsCallBack);
