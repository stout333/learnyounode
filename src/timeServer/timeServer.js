const net = require('net');
const moment = require('moment');

const listener = (socket) => {
  socket.end(`${moment().format('YYYY-MM-DD HH:mm')}\n`);
};

const server = net.createServer(listener);
server.listen(process.argv[2]);
