const fs = require('fs');

const extensionName = `.${process.argv[3]}`;

fs.readdir(process.argv[2], 'utf8', (error, files) => {
  files.forEach((val) => {
    if (val.endsWith(extensionName)) {
      console.log(val);
    }
  });
});
