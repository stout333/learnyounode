const fs = require('fs');
const http = require('http');

const filePath = process.argv[3];

const server = http.createServer((request, response) => {
  const stat = fs.statSync(filePath);

  response.writeHead(200, {
    'content-type': 'text/plain',
    'content-length': stat.size,
  });

  fs.createReadStream(filePath).pipe(response);
});
server.listen(process.argv[2]);
