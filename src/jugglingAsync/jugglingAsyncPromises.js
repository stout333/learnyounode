import http from 'http';

const urls = [...process.argv.slice(2)];

const getData = url => new Promise((resolve, reject) => {
  http.get(url, (res) => {
    let body = '';

    res.on('data', (data) => {
      body += data;
    });

    res.on('end', () => {
      resolve(body);
    });
  }).on('error', (err) => {
    reject(err);
  });
});

Promise.all(urls.map(getData)).then((values) => {
  values.forEach((val) => {
    console.log(val);
  });
});
