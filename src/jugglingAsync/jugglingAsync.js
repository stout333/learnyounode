const http = require('http');

let count = 0;
const dataArray = [];

const printResults = () => {
  dataArray.forEach((val) => {
    console.log(val);
  });
};

const getData = (index) => {
  http.get(process.argv[index + 2], (res) => {
    let body = '';

    res.on('data', (data) => {
      body += data;
    });

    res.on('end', () => {
      dataArray[index] = body;
      count += 1;

      if (count === process.argv.length - 2) {
        printResults();
      }
    });
  });
};

for (let x = 2; x < process.argv.length; x += 1) {
  getData(x - 2);
}
