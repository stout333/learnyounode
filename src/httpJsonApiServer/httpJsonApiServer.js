const http = require('http');
const moment = require('moment');
const url = require('url');

const parseTime = (query) => {
  const date = moment(query.iso);
  const time = {
    hour: date.hour(),
    minute: date.minute(),
    second: date.second(),
  };
  return time;
};

const unixTime = (query) => {
  const date = moment(query.iso);
  const time = {
    unixtime: date.valueOf(),
  };
  return time;
};

const server = http.createServer((request, response) => {
  const urlInfo = url.parse(request.url, true);

  let result;
  if (urlInfo.pathname === '/api/parsetime') {
    result = parseTime(urlInfo.query);
  } else if (urlInfo.pathname === '/api/unixtime') {
    result = unixTime(urlInfo.query);
  }

  if (result) {
    response.writeHead(200, {
      'Content-Type': 'application/json',
    });
    response.end(JSON.stringify(result));
  } else {
    response.writeHead(404);
    response.end();
  }
});
server.listen(process.argv[2]);
